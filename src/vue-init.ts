// NOTE: devtools must be imported before vue
import devtools from '@vue/devtools'; // eslint-disable-line import/no-extraneous-dependencies
import Vue, { VueConstructor, ComponentOptions } from 'vue';
import '@/styles/base.scss';
import { i18n, i18nMarkdown } from '@/i18n';

if (process.env.NODE_ENV === 'development') {
  devtools.connect();
}
Vue.config.productionTip = false;

Vue.mixin({
  methods: {
    $i18n: i18n,
    $i18nm: i18nMarkdown,
  },
});

export default function init(
  app: VueConstructor,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  options: ComponentOptions<any> = {},
  mountElement: Element | string = '#app',
): void {
  new Vue({
    ...options,
    render: h => h(app),
  }).$mount(mountElement);
}
