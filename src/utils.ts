import { debug as debugFn } from 'debug';
import packageJson from '../package.json';

const debugNamespace = packageJson.name;

export function createDebugger(namespace: string): debug.Debugger {
  return debugFn(`${debugNamespace}:${namespace}`);
}

export type WithOptional<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;
export type Include<T, U> = T extends U ? T : never;
export type WantedKeys<T, U> = { [K in keyof T]: T[K] extends U ? K : never }[keyof T];

export const objKeysExact = Object.keys as <T>(o: T) => (Extract<keyof T, string>)[];

export function isUnknownObject(x: unknown): x is { [key in PropertyKey]: unknown } {
  return x !== null && typeof x === 'object';
}

export function clone<T>(obj: T): T {
  return JSON.parse(JSON.stringify(obj));
}

export function delay(ms: number): Promise<unknown> {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
