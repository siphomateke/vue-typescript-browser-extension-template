import Vue, { VueConstructor } from 'vue';
import {
  Args as DefaultArgs,
  Annotations,
  BaseStory,
  BaseMeta,
  BaseDecorators,
} from '@storybook/addons';

type VueComponent = VueConstructor<Vue>;
type VueReturnType = VueComponent;
/**
 * Metadata to configure the stories for a component.
 *
 * @see [Default export](https://storybook.js.org/docs/formats/component-story-format/#default-export)
 */
export type Meta<Component = VueComponent, Args = DefaultArgs> = BaseMeta<Component> & Annotations<Args, VueReturnType>;
/**
 * Story function that represents a component example.
 *
 * @see [Named Story exports](https://storybook.js.org/docs/formats/component-story-format/#named-story-exports)
 */
export type Story<Args = DefaultArgs> = BaseStory<Args, VueReturnType> & Annotations<Args, VueReturnType>;

export type Decorator = BaseDecorators<VueReturnType>[number];
