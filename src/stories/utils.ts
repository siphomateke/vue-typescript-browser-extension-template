import { objKeysExact } from '@/utils';
import { ArgType } from '@storybook/addons';

type ArgTypesObj = Record<string, unknown>;
type ArgTypesKeyed<T extends ArgTypesObj> = Record<keyof T, ArgType>;

export function generateArgTypesFromObj<T extends ArgTypesObj>(obj: T): ArgTypesKeyed<T> {
  const argTypes: ArgTypesKeyed<T> = {} as ArgTypesKeyed<T>;
  objKeysExact(obj).forEach((key) => {
    const value = obj[key];
    let control;
    if (typeof value === 'string') {
      control = 'text';
    } else if (Array.isArray(value)) {
      control = 'array';
    } else if (typeof value === 'number') {
      control = 'number';
    } else if (typeof value === 'object') {
      control = 'object';
    } else {
      throw new Error(`Invalid knob "${key}" of type "${typeof value}"`);
    }
    argTypes[key] = {
      control,
    };
  });
  return argTypes;
}
