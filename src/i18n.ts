import MarkdownIt from 'markdown-it';
import type i18nMessages from '../public/_locales/en/messages.json';

type I18nKeys = keyof typeof i18nMessages;

/**
 * Gets the localized string for the specified message. If the message is missing, this method returns an empty string (''). If the format of the getMessage() call is wrong — for example, messageName is not a string or the substitutions array has more than 9 elements — this method returns undefined.
 * @param messageName The name of the message, as specified in the messages.json file.
 * @param substitutions Substitution strings, if the message requires any.
 * @returns Message localized for current locale.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function i18n(messageName: I18nKeys, ...substitutions: any[]): string {
  return browser.i18n.getMessage(messageName, substitutions.map(v => v.toString()));
}

const md = new MarkdownIt({ html: false });

/**
 * Gets the localized string for the specified message with markdown support. If the message is missing, this method returns an empty string (''). If the format of the getMessage() call is wrong — for example, messageName is not a string or the substitutions array has more than 9 elements — this method returns undefined.
 * @param messageName The name of the message, as specified in the messages.json file.
 * @param substitutions Substitution strings, if the message requires any.
 * @returns Message localized for current locale.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function i18nMarkdown(messageName: I18nKeys, ...substitutions: any[]): string {
  const message = i18n(messageName, ...substitutions);
  return md.render(message);
}
