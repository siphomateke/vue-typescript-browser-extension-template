/* eslint-disable max-classes-per-file */
import { isUnknownObject } from '@/utils';
import { i18n } from '@/i18n';

interface ExtendedErrorJson<P> {
  message: string;
  code: string | null;
  type: string;
  props?: P;
  errorType: 'ExtendedError';
  stack?: string;
}

export class ExtendedError<P = unknown> {
  props?: P;

  error: Error;

  type = '';

  constructor(message: string, public code: string | null = '', props?: P, stack?: string) {
    this.error = new Error(message);
    if (stack) {
      this.error.stack = stack;
    }
    if (props) {
      this.props = props;
    }
    this.setType('ExtendedError');
  }

  get message(): string {
    return this.error.message;
  }

  set message(value: string) {
    this.error.message = value;
  }

  get name(): string {
    return this.error.name;
  }

  setType(type: string): void {
    this.type = type;
    this.error.name = this.type;
  }

  /**
   * Creates an ExtendedError from a JSON representation of one
   */
  static fromJSON<T>(json: ExtendedErrorJson<T>): ExtendedError<T> {
    const error = new ExtendedError(json.message, json.code, json.props, json.stack);
    error.setType(json.type);
    return error;
  }

  /**
   * Converts this error to a JSON object
   */
  toJSON(): ExtendedErrorJson<P> {
    return {
      message: this.message,
      code: this.code,
      type: this.type,
      props: this.props,
      errorType: 'ExtendedError',
      stack: this.error.stack,
    };
  }
}

function isExtendedErrorJson<P>(error: unknown): error is ExtendedErrorJson<P> {
  return isUnknownObject(error) && typeof error.message === 'string' && typeof error.type === 'string';
}

interface GenericJsonErrorJson {
  message: string;
  code?: string;
  stack?: string;
}

export class GenericJsonError extends Error {
  code?: string;

  stack?: string;

  constructor(message: string, code?: string, stack?: string) {
    super(message);
    this.code = code;
    this.stack = stack;
  }

  static fromJSON(json: GenericJsonErrorJson): GenericJsonError {
    return new GenericJsonError(json.message, json.code, json.stack);
  }
}

function isGenericJsonError(error: unknown): error is GenericJsonError {
  return isUnknownObject(error) && 'code' in error;
}

export interface JsonError {
  message: string;
  name?: string;
  code?: string;
  type?: string;
  props?: Record<string, unknown>;
  errorType?: 'ExtendedError';
  stack?: string;
}

/**
 * Converts an error to a JSON object
 */
export function errorToJson(error: unknown): JsonError | null {
  if (error) {
    let output: JsonError = {
      message: '',
    };
    if (isUnknownObject(error)) {
      // Handle ExtendedErrors
      if (typeof error.toJSON === 'function') {
        output = error.toJSON();
      } else if (typeof error.message === 'string') {
        output.message = error.message;
        if (typeof error.stack === 'string') {
          output.stack = error.stack;
        }
      } else {
        output.message = error.toString();
      }
    }
    return output;
  }
  return null;
}

/**
 * Creates an Error from it's JSON representation
 */
export function errorFromJson<P>(json: JsonError | null): ExtendedError<P> | GenericJsonError | Error | null {
  let output = null;
  if (json) {
    if (isExtendedErrorJson(json)) {
      output = ExtendedError.fromJSON(json);
    } else if (json.code) {
      output = GenericJsonError.fromJSON(json);
    } else {
      output = new Error(json.message);
      if (json.stack) {
        output.stack = json.stack;
      }
    }
  }
  return output;
}

/**
 * Converts any error type to a string.
 */
export function errorToString(error: unknown): string {
  let errorString = '';
  if (error instanceof ExtendedError) {
    errorString = `${error.type}: ${error.message}`;
  } else if (isGenericJsonError(error)) {
    errorString = error.message;
  } else if (error instanceof Error) {
    errorString = error.toString();
  } else if (typeof error === 'string') {
    errorString = `Error: ${error}`;
  } else if (isUnknownObject(error)) {
    if (typeof error.message === 'string') {
      errorString = error.message;
    } else {
      errorString = error.toString();
    }
  } else {
    errorString = i18n('generic_error');
  }
  return errorString;
}
