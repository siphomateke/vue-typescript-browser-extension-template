declare module 'sinon-chrome/plugins/i18n' {
  export default class ChromeI18n {
    constructor(translations: Record<string, unknown>);
    /**
     * Install plugin
     */
    install: (chrome: any) => void;
    /**
     * Get message by name and apply provided substitutions
     */
    getMessage: (messageName: string, substitutions: any[]) => string;
    /**
     * Get accept-languages from the browser
     */
    getAcceptLanguages: (callback: function) => void;
    /**
     * Get the browser UI language of the browser
     */
    getUILanguage: () => string;
    /**
     * Detect language from a given string
     */
    detectLanguage: (text: string, callback: function) => void;
  }
}