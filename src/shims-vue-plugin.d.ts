import Vue from 'vue';
import { i18n } from '@/i18n';

export interface VueShimmed {
  $i18n: typeof i18n
}

declare module 'vue/types/vue' {
  interface Vue extends VueShimmed { }
}
