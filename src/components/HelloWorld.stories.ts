import HelloWorld from '@/components/HelloWorld.vue';
import type { Meta, Story } from '@/stories/types';
import Vue from 'vue';

const meta: Meta = {
  title: 'Hello World',
  component: HelloWorld,
};
export default meta;

const Template: Story = (_args, { argTypes }) => Vue.extend({
  components: { HelloWorld },
  props: Object.keys(argTypes),
  template: '<HelloWorld v-bind="$props"/>',
});

export const Default = Template.bind({});
Default.args = {
  msg: 'This is a sample message',
};
