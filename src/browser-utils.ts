import type {
  ExtensionMessageResponse,
  ExtensionMessage,
} from '@/consts';
import { errorFromJson, errorToJson } from '@/errors';
import { createDebugger, isUnknownObject } from '@/utils';

const debug = createDebugger('utils:warning');

export async function getFromStorage<T>(key: string, sync = false): Promise<undefined | T> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let items: Record<string, any>;
  if (sync) {
    items = await browser.storage.sync.get(key);
  } else {
    items = await browser.storage.local.get(key);
  }
  if (items) {
    return items[key];
  }
  return undefined;
}

export function setStorage(items: Record<string, unknown>, sync = false): Promise<void> {
  if (sync) {
    return browser.storage.sync.set(items);
  }
  return browser.storage.local.set(items);
}

type ExtensionMessageErrorResponse = { error: ReturnType<typeof errorToJson> };

function messageResponseIsError(response: unknown): response is ExtensionMessageErrorResponse {
  return isUnknownObject(response) && 'error' in response;
}

interface ExtensionMessageWithData<M extends ExtensionMessage> { data: M }

export async function sendMessage<
  M extends ExtensionMessage,
  R extends ExtensionMessageResponse<M>,
>(message: M, tabId?: number): Promise<R> {
  let response: unknown;
  const messageToSend: ExtensionMessageWithData<M> = { data: message };
  if (tabId) {
    response = await browser.tabs.sendMessage(tabId, messageToSend);
  } else {
    response = await browser.runtime.sendMessage(messageToSend);
  }
  if (messageResponseIsError(response)) {
    // eslint-disable-next-line @typescript-eslint/no-throw-literal
    throw errorFromJson(response.error);
  }
  return response as R;
}

type ExtensionMessageHandler<T> = (message: unknown, sender: browser.runtime.MessageSender) => Promise<T | ExtensionMessageErrorResponse> | void;
type ExtensionObjectMessageHandler<M extends ExtensionMessage, T> = (message: M, sender: browser.runtime.MessageSender) => Promise<T> | void;

function extensionMessageHasData<M extends ExtensionMessage>(message: unknown): message is ExtensionMessageWithData<M> {
  return isUnknownObject(message) && typeof message.data === 'object';
}

function isPromise<T>(obj: unknown): obj is Promise<T> {
  return isUnknownObject(obj) && typeof obj.then === 'function';
}

/**
 *
 * @returns `unsubscribe()` function to stop listening for messages.
 */
export function onExtensionMessage<M extends ExtensionMessage, T>(callback: ExtensionObjectMessageHandler<M, T>): () => void {
  const wrappedCallback: ExtensionMessageHandler<T> = (message, sender) => {
    if (sender.id === browser.runtime.id && typeof message === 'object' && !!message) {
      if (extensionMessageHasData<M>(message)) {
        const callbackData = callback(message.data, sender);
        if (isPromise(callbackData)) {
          return callbackData
            // Don't throw error because the error code will be lost
            .catch(error => ({ error: errorToJson(error) }));
        }
        if (typeof callbackData !== 'undefined') {
          debug(`Expected extension message listener to return a promise. Got: "${typeof callbackData}"`);
        }
      }
      // If there was no data in the message, the message might have encountered an error.
      // We don't handle said error though because this is just for listening to incoming data.
    }
    return undefined;
  };
  function unsubscribe(): void {
    browser.runtime.onMessage.removeListener(wrappedCallback);
  }
  browser.runtime.onMessage.addListener(wrappedCallback);
  return unsubscribe;
}

export function waitForMessage(test: (message: ExtensionMessage) => boolean): Promise<void> {
  return new Promise((resolve) => {
    const unsubscribe = onExtensionMessage((message) => {
      if (test(message)) {
        unsubscribe();
        resolve();
      }
    });
  });
}

/**
 * Checks if this JavaScript context is running in a browser tab.
 */
export function selfIsTab(): boolean {
  return !browser.tabs;
}

export function getAsset(path: string): string {
  if (!path.includes('data:')) {
    return browser.runtime.getURL(path);
  }
  return path;
}
