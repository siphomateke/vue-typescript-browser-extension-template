export enum ExtensionMessageType {
  // name = 'NAME'
}

export type ExtensionBaseMessages = {
  // name: Record<string, unknown> | undefined;
};

export type ExtensionMessageTypes = keyof ExtensionBaseMessages;
export type ExtensionMessages = {
  [T in keyof ExtensionBaseMessages]: { type: T } & ExtensionBaseMessages[T];
};
export type ExtensionMessage = ExtensionMessages[keyof ExtensionMessages];
export type ExtensionMessageFromType<C extends ExtensionMessageTypes> = ExtensionMessages[C];

export type ExtensionMessageResponses<M extends ExtensionMessage> = {
  // name: void;
};

export type ExtensionMessageResponse<M extends ExtensionMessage> = ExtensionMessageResponses<M>[M['type']];
