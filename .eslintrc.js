module.exports = {
  root: true,
  env: {
    browser: true,
    webextensions: true,
  },
  parserOptions: {
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
  },
  extends: [
    '@siphomateke/eslint-config-typescript',
    '@vue/typescript/recommended',
    'plugin:vue/recommended',
  ],
  settings: {
    // Copied from eslint-config-airbnb https://github.com/vuejs/eslint-config-airbnb/blob/cfa98d5f95be585d72e092f783178518cf3c0aa5/index.js
    'import/resolver': {
      // https://github.com/benmosher/eslint-plugin-import/issues/1396
      [require.resolve('eslint-import-resolver-node')]: {},
      [require.resolve('eslint-import-resolver-webpack')]: {
        config: require.resolve('@vue/cli-service/webpack.config.js'),
      },
    },
  },
  overrides: [
    {
      files: ['*.stories.ts', 'src/stories/**/*.ts'],
      rules: {
        'import/no-extraneous-dependencies': 'off',
      },
    },
    {
      files: ['src/mixins/**/*.ts'],
      rules: {
        '@typescript-eslint/explicit-function-return-type': 'off',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
      },
    },
    {
      files: ['src/store/**/*.ts'],
      rules: {
        '@typescript-eslint/explicit-module-boundary-types': 'off',
      },
    },
  ],
};
