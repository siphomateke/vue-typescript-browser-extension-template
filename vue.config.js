const fs = require('fs');
const path = require('path');
const packageJson = require('./package.json');

const browser = process.env.BROWSER ? process.env.BROWSER : 'chrome';

/**
 * @type {import("@vue/cli-service").ProjectOptions}
 */
module.exports = {
  lintOnSave: false,
  productionSourceMap: false,
  filenameHashing: false,
  pages: {
    app: {
      entry: 'src/pages/app/index.ts',
      template: 'src/pages/app/index.html',
    },
  },
  chainWebpack: (config) => {
    config
      .plugin('define')
      .tap((args) => {
        args[0]['process.env'].BROWSER = `"${browser}"`;
        args[0]['process.env'].DEBUG = `"${process.env.DEBUG}"`;
        return args;
      });

    config.optimization.minimizer('terser').tap((args) => {
      if (typeof args[0].terserOptions.output === 'undefined') {
        args[0].terserOptions.output = {};
      }
      args[0].terserOptions.output.ascii_only = true;
      return args;
    });

    // Fix for Firefox opening windows while attempting to launch "webpack:///" URLs
    if (process.env.NODE_ENV === 'development' && browser === 'firefox') {
      config.devtool('source-map');
    }
  },
  pluginOptions: {
    browserExtension: {
      artifactFilename: ({ name, version, mode }) => {
        if (mode === 'production') {
          return `${name}-v${version}-${browser}.zip`;
        }
        return `${name}-v${version}-${browser}-${mode}.zip`;
      },
      api: 'browser',
      usePolyfill: true,
      autoImportPolyfill: true,
      componentOptions: {
        background: {
          entry: 'src/extension/background.ts',
        },
        contentScripts: {
          entries: {
            'content-script': 'src/extension/content-script.ts'
          },
        },
      },
      manifestTransformer(originalManifest) {
        const manifest = Object.assign({}, originalManifest);
        if (browser === 'chrome') {
          if (manifest.applications) {
            delete manifest.applications;
          }
          if (manifest.browser_specific_settings) {
            delete manifest.browser_specific_settings;
          }
          if (manifest.sidebar_action) {
            delete manifest.sidebar_action;
          }
          if (manifest.browser_action && 'browser_style' in manifest.browser_action) {
            delete manifest.browser_action.browser_style;
          }
          if (manifest.options_ui && 'browser_style' in manifest.options_ui) {
            delete manifest.options_ui.browser_style;
            manifest.options_ui.chrome_style = manifest.options_ui.browser_style;
          }
        }

        const fullVersion = packageJson.version;
        const numericVersion = fullVersion.split('-')[0];
        manifest.version = numericVersion;

        // Firefox doesn't support the 'version_name' key
        if (browser === 'chrome') {
          manifest.version_name = fullVersion;
        }

        return manifest;
      },
    },
  },
  outputDir: `dist/${browser}`,
};
