import { ADDON_ID as controlsAddonPanelId } from '@storybook/addon-controls/dist/constants'
import { addons } from '@storybook/addons';

addons.setConfig({
  selectedPanel: controlsAddonPanelId,
});
