import './style.scss';
import '@/vue-init';
import browser from 'sinon-chrome';
import I18nPlugin from 'sinon-chrome/plugins/i18n';
import translations from '../public/_locales/en/messages.json';
import extensionManifest from '@/manifest.json';

browser.registerPlugin(new I18nPlugin(translations));
browser.runtime.getManifest.returns(extensionManifest);
browser.runtime.getURL.callsFake(p => p);

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}