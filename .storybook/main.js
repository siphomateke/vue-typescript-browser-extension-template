const webpack = require('webpack');

/** @type {import(@storybook/core/types').StorybookConfig} */
const config = {
  stories: [
    '../src/**/*.stories.mdx',
    '../src/**/*.stories.@(js|jsx|ts|tsx)'
  ],
  addons: [
    '@storybook/addon-essentials',
  ],
  webpackFinal: (config) => {
    if (!config.plugins) config.plugins = [];
    config.plugins.push(...[
      new webpack.ProvidePlugin({
        browser: 'sinon-chrome',
      }),
    ]);
    return config;
  },
};
module.exports = config;