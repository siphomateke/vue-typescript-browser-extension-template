# Vue.js TypeScript Browser Extension Template

Template for creating browser extensions with Vue.js, TypeScript and Storybook.

## Installation

Until this extension is published, installation must be done using manual development tools.

To install the extension, first download the latest stable release. Then if you are using Chrome, go to extensions, toggle developer mode on the top right and then drag and drop the zip file to anywhere on that page. To install the extension in Firefox, go to `about:debugging#/runtime/this-firefox`, press "Load Temporary Add-on..." and select the downloaded zip file.

The extension should now be successfully installed and ready to use.

## Building

```bash
yarn run serve:chrome
yarn run serve:firefox

# Build for production
yarn run build:chrome
yarn run build:firefox
yarn run build:all # Build for all supported browsers

# Lint codebase to discover errors and warnings
yarn run lint
```
